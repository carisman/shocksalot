module Mesh

using LinearAlgebra
export node_format, face_format, cell_format, boundary_format, mesh_format, Read_openFOAM_Mesh!
#Mesh data structures
#not sure if these need to be mutable or not

#import base operators for overloading
import Base: -,+,*,/,^,copy

mutable struct node_format
    id::Int64
    centroid::Vector{Float64}
    #constructors
    function node_format(id::Int64, coords::Vector{Float64})
        nCoords = length(coords)
        if(nCoords == 3)
            new(id, coords)
        else
            ErrorException("Attempted to create node with $nCoords coordinates")
        end
    end
end

mutable struct cell_format{T}
    id::Int64
    faces::Vector{T}
    centroid::Vector{Float64}
    #thermo_vars::Array{thermo_format,1}
    nodes::Vector{node_format}
    neighs::Vector{cell_format}
end

mutable struct face_format
    id::Int64
    nodes::Vector{node_format}
    normal::Vector{Float64}
    centroid::Vector{Float64}
    IP::Vector{Float64}
    area::Float64
    #julia doesn't allow for cross dependent types?
    #using IDs for now, but ref to cells would be better
    own::cell_format
    neigh::cell_format
    #constructors
    face_format() = new()
    function face_format(id::Int64,nodes::Vector{node_format})
        temp = new(id,nodes)

    end
end

#cell constructors need to be defined after face format as they depend on the
#construct cell with empty arrays and only id defined
function cell_format{face_format}(id::Int64)
    nodes = Vector{node_format}()
    neighs = Vector{cell_format}()
    faces = Vector{face_format}()
    centroid = [0,0,0]
    temp = cell_format{face_format}(id,faces,centroid,nodes,neighs)
    return temp
end

#supposedly this helps performance so note to switch this later
#mutable struct boundary_format{T<:Function}
mutable struct boundary_format
    id::Int64
    name::String
    b_type::String
    nFaces::Int64
    startFace::Int64
    cells::Vector{cell_format}
    updateFunction::Function
    boundary_format(id::Int64) = new(id)
end

mutable struct mesh_format
    id::Int64
    nodes::Vector{node_format}
    faces::Vector{face_format}
    cells::Vector{cell_format}
    boundaries::Vector{boundary_format}
    boundary_cells::Dict{Int64,cell_format}
    empty_faces::Vector{face_format}
    #constructors
    mesh_format(id::Int64) = new(id)
end

abstract type mesh_value
end
#variables that live on the mesh
#note that we're assuming face, cell ID = index in mesh array
#if we were good about defining everything in the mesh init this is valid
#otherwise we need to convert the mesh storage to a dict later on
#not sure how to handle face values yet
mutable struct mesh_scalar <: mesh_value
    #reference to the mesh
    mesh::mesh_format
    #arrays for the faces, cells
    cellVals::Vector{Float64}
    faceVals::Vector{Float64}
    #constructor
    mesh_scalar(mesh::mesh_format, cellVals::Vector{Float64}, faceVals::Vector{Float64}) = new(mesh,cellVals,faceVals)
    function mesh_scalar(mesh::mesh_format, value::Float64)
        temp = new(mesh)
        temp.cellVals = zeros(length(mesh.cells)).+value
        temp.faceVals = zeros(length(mesh.faces)).+value
        return temp
    end
end

#julias a bit of a pain in that 1D arrays are columns, not rows
#I think we might need to build a vector class to make things easier
#to manage. Currently need to transpose a single vector to multiply through
mutable struct mesh_vector <: mesh_value
    #reference to the mesh
    mesh::mesh_format
    #arrays for the faces, cells
    cellVals::Array{Float64}
    faceVals::Array{Float64}
    #constructor
    mesh_vector(mesh::mesh_format, cellVals::Array{Float64}, faceVals::Array{Float64}) = new(mesh,cellVals,faceVals)
    function mesh_vector(mesh::mesh_format, value::Vector{Float64})
        temp = new(mesh)
        temp.cellVals = zeros(length(mesh.cells),3)
        temp.faceVals = zeros(length(mesh.faces),3)
        temp.cellVals.+transpose(value)
        return temp
    end
end

#copy functions required for operators
#origionally was making copies but wasn't working as intended, just makes a new empty var
function copy(x::mesh_vector)
    temp = mesh_vector(x.mesh,zeros(1,3))
    return temp
end

function copy(x::mesh_scalar)
    temp = mesh_scalar(x.mesh,0.0)
    return temp
end

#update functions update the value of the first arg by the 2nd
#(for mesh var functions) could use shorter names for these
#or even operators that make sense (i'd like to use +! but that dun work i guess)
#not even sure these are any more efficient but they're here
function updatePlus!(x::mesh_value,y::mesh_value)
    x.cellVals .= x.cellVals .+ y.cellVals
    return x
end

function updateMult!(x::mesh_value,y::mesh_value)
    x.cellVals = x.cellVals .* y.cellVals
    return x
end

function updateDivide!(x::mesh_value,y::mesh_value)
    x.cellVals .= x.cellVals ./ y.cellVals
    return x
end

function updatePlus!(x::mesh_value,y::Float64)
    x.cellVals .= x.cellVals .+ y
    return x
end

function updateMult!(x::mesh_value,y::Float64)
    x.cellVals .= x.cellVals .* y
    return x
end

function updateDivide!(x::mesh_value,y::Float64)
    x.cellVals .= x.cellVals ./ y
    return x
end

function updateExp!(x::mesh_value,y::Float64)
    x.cellVals .= x.cellVals .^ y
    return x
end

#abstract types should take care of all this right??????
#not sure if this is slower? idk its way less code tho!
#also not sure the correct behaviour here, currently we make a new
#mesh var and return that
function *(x::mesh_value,y::mesh_value)
    temp = copy(x)
    temp.cellVals .= x.cellVals .* y.cellVals
    return temp
end

function +(x::mesh_value,y::mesh_value)
    temp = copy(x)
    temp.cellVals .= x.cellVals .+ y.cellVals
    return temp
end

function -(x::mesh_value,y::mesh_value)
    temp = copy(x)
    temp.cellVals .= x.cellVals .- y.cellVals
    return temp
end

function /(x::mesh_value,y::mesh_value)
    temp = copy(x)
    temp.cellVals .= x.cellVals ./ y.cellVals
    return temp
end

function *(x::mesh_value,y::Float64)
    temp = copy(x)
    temp.cellVals .= x.cellVals .* y
    return temp
end

function *(x::Float64,y::mesh_value)
    temp = copy(y)
    temp.cellVals .= x .* y.cellVals
    return temp
end

function +(x::mesh_value,y::Float64)
    temp = copy(x)
    temp.cellVals .= x.cellVals .+ y
    return temp
end

function /(x::mesh_value,y::Float64)
    temp = copy(x)
    temp.cellVals .= x.cellVals ./ y
    return temp
end

function ^(x::mesh_value,y::Float64)
    temp = copy(x)
    temp.cellVals .= x.cellVals .^ y
    return temp
end

# #these get a bit funky with the lin-alg and behaviours and row/column, not sure
# if we ever need them though but its good to have transpose taken care of here
function *(x::mesh_vector,y::Vector{Float64})
    temp = copy(x)
    temp.cellVals .= x.cellVals .* transpose(y)
    return temp
end

function /(x::mesh_vector,y::Vector{Float64})
    temp = copy(x)
    temp.cellVals .= x.cellVals ./ transpose(y)
    return temp
end

function +(x::mesh_vector,y::Vector{Float64})
    temp = copy(x)
    temp.cellVals .= x.cellVals .+ transpose(y)
    return temp
end

function /(x::mesh_vector,y::mesh_scalar)
    temp = mesh_vector(x.mesh,[0.0,0.0,0.0])
    temp.cellVals[:,1] .= x.cellVals[:,1] ./ y.cellVals
    temp.cellVals[:,2] .= x.cellVals[:,2] ./ y.cellVals
    temp.cellVals[:,3] .= x.cellVals[:,3] ./ y.cellVals
    return temp
end

function magSqr(x::mesh_vector)
    temp = mesh_scalar(x.mesh,0.0)
    temp.cellVals .= x.cellVals[:,1] .* x.cellVals[:,1]  .+ x.cellVals[:,2] .* x.cellVals[:,2] .+ x.cellVals[:,3] .* x.cellVals[:,3]
    return temp
end



#reading functions, can prob be put in a diff module
function read_points_file!(nodes::Vector{node_format})
    f = open("polyMesh/points")
    lines = readlines(f)
    close(f)
    #beepboopblepbop
    startLine = false
    id = 1
    for line in lines
        #start reading conditional
        if(line == "(" && startLine == false)
            startLine = true
            continue
        end
        #stop reading conditional
        if(line == ")")
            return
        end
        if(startLine == true)
            line = replace(line,"(" => "")
            line = replace(line,")" => "")
            line = split(line)
            push!(nodes,node_format(id,map(x->parse(Float64,x),line)))
            id += 1
        end
    end
end

function read_faces_file!(nodes::Vector{node_format}, faces::Vector{face_format})
    f = open("polyMesh/faces")
    lines = readlines(f)
    close(f)
    startLine = false
    id = 1
    for line in lines
        #start reading conditional
        if(line == "(" && startLine == false)
            startLine = true
            continue
        end
        #stop reading conditional
        if(line == ")")
            return
        end
        if(startLine == true)
            line = replace(line,"(" => " ")
            line = replace(line,")" => "")
            line = split(line)
            line = map(x->parse(Int64,x),line)
            temp_node_list = Vector{node_format}()
            nNodes = line[1]
            for i in range(2,length = nNodes)
                #note that openfoam nodes are 0 indexed
                push!(temp_node_list,nodes[line[i]+1])
            end
            push!(faces,face_format(id,temp_node_list))
            id += 1
        end
    end
end

function read_connectivity!(faces::Vector{face_format}, cells::Vector{cell_format})
    #read owner file
    f = open("polyMesh/owner")
    lines = readlines(f)
    close(f)
    startLine = false
    owners = Vector{Int64}()
    for line in lines
        #start reading conditional
        if(line == "(" && startLine == false)
            startLine = true
            continue
        end
        #stop reading conditional
        if(line == ")")
            break
        end
        if(startLine == true)
            push!(owners,parse(Int64,line))
        end
    end

    f = open("polyMesh/neighbour")
    lines = readlines(f)
    close(f)
    startLine = false
    neighbours = Vector{Int64}()
    for line in lines
        #start reading conditional
        if(line == "(" && startLine == false)
            startLine = true
            continue
        end
        #stop reading conditional
        if(line == ")")
            break
        end
        if(startLine == true)
            push!(neighbours,parse(Int64,line))
        end
    end

    #initialize cells
    nCells = max(maximum(owners),maximum(neighbours))+1
    for i in range(1, length = nCells)
        push!(cells,cell_format{face_format}(i))
        #cells[i].faces = Vector{face_format}()
    end

    #setup cell face arrays and own/neigh information
    for i in range(1, length = length(owners))
        #push!(cells[owners[i]+1].faces,faces[i])
        faces[i].own = cells[owners[i]+1]
    end

    for i in range(1, length = length(neighbours))
        #push!(cells[neighbours[i]+1].faces,faces[i])
        faces[i].neigh = cells[neighbours[i]+1]
        #push!(faces[i].own.neighs, faces[i].neigh)
    end

    #setup cell neigh arrays
    for face in faces
        push!(face.own.faces,face)
        #stop at end of neigh array, boundary definitions begin there
        if face.id <= length(neighbours)
            push!(face.neigh.faces,face)
            push!(face.own.neighs,face.neigh)
            push!(face.neigh.neighs,face.own)
        end
    end
end
import Boundary
function read_boundary_file!(mesh::mesh_format)
    f = open("polyMesh/boundary")
    lines = readlines(f)
    close(f)
    startLine = false
    mesh.boundaries = Vector{boundary_format}()
    id = 1
    for i in 1:length(lines)
        line = lines[i]
        #start reading conditional
        if(line == "(" && startLine == false)
            startLine = true
            continue
        end
        #stop reading conditional
        if(line == ")")
            break
        end
        if(startLine == true)
            line = split(line)
            #create new boundary condition
            if line[1] == "{"
                push!(mesh.boundaries,boundary_format(id))
                mesh.boundaries[id].name = replace(lines[i-1]," " => "")
            end
            if line[1] == "type"
                mesh.boundaries[id].b_type = replace(line[2]," " => "")
                mesh.boundaries[id].b_type = replace(mesh.boundaries[id].b_type,";" => "")
            end
            if line[1] == "nFaces"
                nFaces = replace(line[2],";" => "")
                mesh.boundaries[id].nFaces = parse(Int64,nFaces)
            end
            if line[1] == "startFace"
                startFace = replace(line[2],";" => "")
                mesh.boundaries[id].startFace = parse(Int64,startFace)
            end
            if line[1] == "}"
                id += 1
            end
        end
    end

    #create boundary cells
    #boundary cells are cells with 1 face, and no centroid/nodes
    #note that faces are indexed from 0 in openFOAM
    id = length(mesh.cells)+1
    #bcells have negative id
    #id = -1
    mesh.boundary_cells = Dict{Int64,Float64}()
    mesh.empty_faces = Vector{face_format}()
    for boundary in mesh.boundaries
        if boundary.b_type != "empty"
            boundary.cells = Vector{cell_format}()
            for i in range(boundary.startFace+1,length = boundary.nFaces)
                tempCell = cell_format{face_format}(id)
                push!(tempCell.neighs,mesh.faces[i].own)
                push!(tempCell.faces,mesh.faces[i])
                mesh.faces[i].neigh = tempCell
                mesh.boundary_cells[id] = tempCell
                push!(boundary.cells,tempCell)
                push!(mesh.cells,tempCell)
                id += 1
            end
        else
            for i in range(boundary.startFace+1,length = boundary.nFaces)
                push!(mesh.empty_faces,mesh.faces[i])
            end
            deleteat!(mesh.faces,range(boundary.startFace+1,length = boundary.nFaces))
        end
    end
    set_BCs!(mesh)
end

#currently hard coded boundaries for test case
function set_BCs!(mesh::mesh_format)
    mesh.boundaries[1].updateFunction = Boundary.constValue
    mesh.boundaries[2].updateFunction = Boundary.internalValue
    mesh.boundaries[3].updateFunction = Boundary.internalValue
    mesh.boundaries[4].updateFunction = Boundary.internalValue
    mesh.boundaries[5].updateFunction = Boundary.noSlipWall
end

function set_face_properties!(face::face_format)
    #find face centroids by averaging node values
    #TODO: need to impliment a better way to do this
    centroid = [0, 0, 0]
    for node in face.nodes
        centroid += node.centroid
    end
    face.centroid = centroid / length(face.nodes)
    #set integration point to be centroid
    #TODO: need to actually calculate integration point for skew faces
    face.IP = face.centroid

    #find face normals via cross product
    #TODO: make this do an average of all the nodes???? lin alg sucks
    #TODO: make sure boundary normals face out? i guess?
    ab = (face.nodes[1].centroid - face.centroid)
    ac = (face.nodes[2].centroid - face.centroid)
    face.normal = cross(ab,ac)
    face.normal = face.normal / norm(face.normal)

    #calculate face areas
    #not sure if this is right, works for x-y oriented faces so far
    #TODO:Check this? or whatever
    total = [0.0, 0.0, 0.0]
    for i in range(2,stop = length(face.nodes) - 1)
        total += cross(face.nodes[i].centroid - face.nodes[1].centroid , face.nodes[i+1].centroid - face.nodes[1].centroid)
    end
    area = norm(total)
    face.area = 0.5 * area
end

function setup_mesh_elem_properties!(mesh::mesh_format)
    for cell in mesh.cells
        for face in cell.faces
            #set face centroid, area, normal
            set_face_properties!(face)
            #collect unique list of cell nodes
            for node in face.nodes
                if !in(node,cell.nodes)
                    push!(cell.nodes, node)
                end
            end
        end
    end
    #find cell centroids by averaging node values
    #TODO: need to impliment a better way to do this
    for cell in mesh.cells
        centroid = [0, 0, 0]
        for node in cell.nodes
            centroid += node.centroid
        end
        cell.centroid = centroid / length(cell.nodes)
    end
end



function Read_openFOAM_Mesh!(mesh::mesh_format)
    #initialize mesh vectors (shoudl prob be in the constructor)
    mesh.nodes = Vector{node_format}()
    mesh.faces = Vector{face_format}()
    mesh.cells = Vector{cell_format}()
    read_points_file!(mesh.nodes)
    read_faces_file!(mesh.nodes,mesh.faces)
    read_connectivity!(mesh.faces, mesh.cells)
    read_boundary_file!(mesh)
    setup_mesh_elem_properties!(mesh)
end

end
