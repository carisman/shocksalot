module Flux

import Mesh
import Thermo
using LinearAlgebra

export rho_Flux!

function update_flux!(thermo::Thermo.thermoType, mesh::Mesh.mesh_format)
    for faceIter in mesh.faces
        Flux.rho_Flux!(faceIter.own.id,faceIter.neigh.id,faceIter.id,thermo,mesh)
    end
end

function magSqr(x::Array{Float64})
    temp = 0.0
    for iter in x
        temp += iter*iter
    end
    return temp
end

function rho_Flux!(CL::Int64, CR::Int64, face::Int64, thermo::Thermo.thermoType, mesh::Mesh.mesh_format)

    SMALL = 1e-10

    area = mesh.faces[face].area
    normalVector = mesh.faces[face].normal

    pLeft = thermo.P.cellVals[CL]
    pRight = thermo.P.cellVals[CR]

    ULeft = thermo.U.cellVals[CL,:]
    URight = thermo.U.cellVals[CR,:]

    TLeft = thermo.T.cellVals[CL]
    TRight = thermo.T.cellVals[CR]

    rhoLeft = thermo.rho.cellVals[CL]
    rhoRight = thermo.rho.cellVals[CR]

    eLeft = thermo.h.cellVals[CL] + 0.5 * magSqr(ULeft)
    eRight = thermo.h.cellVals[CR] + 0.5 * magSqr(ULeft)

    CvLeft = thermo.fluid.Cv
    CvRight = thermo.fluid.Cp
    RLeft = thermo.fluid.R
    RRight = thermo.fluid.R

    kappaLeft = (CvLeft + RLeft)/CvLeft
    kappaRight = (CvRight + RRight)/CvRight

    contrVLeft  = dot(ULeft,normalVector)
    contrVRight = dot(URight,normalVector)

    hLeft = eLeft + pLeft/rhoLeft
    hRight = eRight + pRight/rhoRight

    rhoTilde = sqrt(max(rhoLeft*rhoRight, SMALL))
    rhoLeftSqrt = sqrt(max(rhoLeft, SMALL))
    rhoRightSqrt = sqrt(max(rhoRight, SMALL))

    wLeft = rhoLeftSqrt/(rhoLeftSqrt + rhoRightSqrt)
    wRight = 1 - wLeft

    UTilde = ULeft*wLeft + URight*wRight
    hTilde = hLeft*wLeft + hRight*wRight
    qTildeSquare = magSqr(UTilde)
    kappaTilde = kappaLeft*wLeft + kappaRight*wRight

    # Speed of sound
    cTilde = sqrt(max((kappaTilde - 1)*(hTilde - 0.5*qTildeSquare), SMALL))

    #Roe averaged contravariant velocity
    contrVTilde = dot(UTilde,normalVector)

    #Step 3: compute primitive differences:
    deltaP = pRight - pLeft
    deltaRho = rhoRight - rhoLeft
    deltaU = URight - ULeft
    deltaContrV = dot(deltaU,normalVector)

    #Step 4: compute wave strengths:

#Roe and Pike - formulation
    r1 = (deltaP - rhoTilde*cTilde*deltaContrV)/(2.0*cTilde^2)
    r2 = deltaRho - deltaP/cTilde^2
    r3 = (deltaP + rhoTilde*cTilde*deltaContrV)/(2.0*cTilde^2)

    #Step 5: compute l vectors

    #rho row:
    l1rho = 1
    l2rho = 1
    l3rho = 0
    l4rho = 1

    #first U column
    l1U = UTilde - cTilde*normalVector

    #second U column
    l2U = UTilde

    # third U column
    l3U = deltaU - deltaContrV*normalVector

    # fourth U column
    l4U = UTilde + cTilde*normalVector

    # E row
    l1e = hTilde - cTilde*contrVTilde
    l2e = 0.5*qTildeSquare
    l3e = (UTilde & deltaU) - contrVTilde*deltaContrV
    l4e = hTilde + cTilde*contrVTilde

    #Step 6: compute eigenvalues

    #derived from algebra by hand, only for Euler equation usefull
    lambda1 = mag(contrVTilde - cTilde)
    lambda2 = mag(contrVTilde)
    lambda3 = mag(contrVTilde + cTilde)

    #Step 7a: Alternative entropy correction: Felipe Portela, 9/Oct/2013
    UL = dot(ULeft , normalVector)
    UR = dot(URight , normalVector)
    cLeft = sqrt(max((kappaLeft - 1)*(hLeft - 0.5*magSqr(ULeft)),SMALL))

    cRight = sqrt(max((kappaRight - 1)*(hRight - 0.5*magSqr(URight)),SMALL))

    #First eigenvalue: U - c
    eps = 2*max(0,(UR - cRight) - (UL - cLeft));
    if (lambda1 < eps)
        lambda1 = (sqr(lambda1) + sqr(eps))/(2.0*eps);
    end

    #Second eigenvalue: U
    eps = 2*max(0, UR - UL);
    if (lambda2 < eps)
        lambda2 = (sqr(lambda2) + sqr(eps))/(2.0*eps);
    end

    #Third eigenvalue: U + c
    eps = 2*max(0,(UR + cRight) - (UL + cLeft));
    if (lambda3 < eps)
        lambda3 = (sqr(lambda3) + sqr(eps))/(2.0*eps);
    end


    #Step 8: Compute flux differences

    #Components of deltaF1
    diffF11 = lambda1*r1*l1rho;
    diffF124 = lambda1*r1*l1U;
    diffF15 = lambda1*r1*l1e;

    #Components of deltaF2
    diffF21 = lambda2*(r2*l2rho + rhoTilde*l3rho);
    diffF224 = lambda2*(r2*l2U + rhoTilde*l3U);
    diffF25 = lambda2*(r2*l2e + rhoTilde*l3e);

    #Components of deltaF3
    diffF31 = lambda3*r3*l4rho;
    diffF324 = lambda3*r3*l4U;
    diffF35 = lambda3*r3*l4e;

    #Step 9: compute left and right fluxes

    #Left flux 5-vector
    fluxLeft11 = rhoLeft*contrVLeft;
    fluxLeft124 = ULeft*fluxLeft11 + normalVector*pLeft;
    fluxLeft15 = hLeft*fluxLeft11;

    #Right flux 5-vector
    fluxRight11 = rhoRight*contrVRight;
    fluxRight124 = URight*fluxRight11 + normalVector*pRight;
    fluxRight15 = hRight*fluxRight11;

    # Step 10: compute face flux 5-vector
    flux1 = 0.5*(fluxLeft11 + fluxRight11 - (diffF11 + diffF21 + diffF31));

    flux24 = 0.5*(fluxLeft124 + fluxRight124 - (diffF124 + diffF224 + diffF324));

    flux5 = 0.5*(fluxLeft15 + fluxRight15 - (diffF15 + diffF25 + diffF35));

    #Compute private data
    rhoFlux  = flux1*area;
    rhoUFlux = flux24*area;
    rhoEFlux = flux5*area;

    thermo.drho.faceVals[face] = rhoFlux
    thermo.drhoU.faceVals[face] = rhoUFlux
    thermo.drhoE.faceVals[face] = rhoEFlux

end



end
