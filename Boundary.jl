module Boundary

import Mesh
import Thermo

export constValue

#boundary cells get their values overwritten when update boundaries is called, this is done after
#the main mesh thermodynamic cells are updated

#need to set these somehow
function constValue(mesh::Mesh.mesh_format,thermo::Thermo.thermoType,cell::Int64)
    thermo.T.cellVals[cell] = 300
    thermo.rho.cellVals[cell] = 1.8
    thermo.U.cellVals[cell,:] = [1735,0.0,0.0]

    thermo.P.cellVals[cell] = thermo.rho.cellVals[cell] * thermo.T.cellVals[cell] * thermo.fluid.R

    return
end
#ideally this could use pointers but I think copying vals is faster
function internalValue(mesh::Mesh.mesh_format,thermo::Thermo.thermoType,cell::Int64)
    thermo.T.cellVals[cell] = thermo.T.cellVals[mesh.boundary_cells[cell].neighs[1].id]
    thermo.P.cellVals[cell] = thermo.P.cellVals[mesh.boundary_cells[cell].neighs[1].id]
    thermo.rho.cellVals[cell] = thermo.rho.cellVals[mesh.boundary_cells[cell].neighs[1].id]

    thermo.U.cellVals[cell,:] = thermo.U.cellVals[mesh.boundary_cells[cell].neighs[1].id,:]
    return
end

function noSlipWall(mesh::Mesh.mesh_format,thermo::Thermo.thermoType,cell::Int64)
    #internal T, P, rho (adiabatic wall)
    thermo.T.cellVals[cell] = thermo.T.cellVals[mesh.boundary_cells[cell].neighs[1].id]
    thermo.P.cellVals[cell] = thermo.P.cellVals[mesh.boundary_cells[cell].neighs[1].id]
    thermo.rho.cellVals[cell] = thermo.rho.cellVals[mesh.boundary_cells[cell].neighs[1].id]

    thermo.U.cellVals[cell,:] = zeros(1,3)
    return
end
end
