module Thermo

import Mesh
using LinearAlgebra
export rho_ideal_gas, fluid_const, update_thermo!

#mu and k are for later, this one is for constant fluid properties
#or have parametarized types with pointers to functions???? idk
#there should be a super cool neat way to make any of these be definable
Ru = 8314.4598

mutable struct fluid_const
    MW::Float64
    gamma::Float64
    mu::Float64
    k::Float64
    R::Float64
    Cp::Float64
    Cv::Float64
    #constructor
    function fluid_const(MW::Float64,gamma::Float64,mu::Float64,k::Float64)
        temp = new(MW,gamma,mu,k,Ru/MW)
        temp.Cp = temp.R*gamma/(gamma-1)
        temp.Cv = temp.Cp - temp.R
        return temp
    end
end

abstract type thermoType
end
#these are intended to be inter-face like where an update function
#should be defined
#not sure the best way to organize the different options
#eg. polynomials, viscid, inviscid etc.
#calculates rho and T and gets P from ideal gas law
mutable struct rho_ideal_gas <: thermoType
    #fluid props, should be abstract type
    fluid::fluid_const
    #scalars
    T::Mesh.mesh_scalar
    P::Mesh.mesh_scalar
    U::Mesh.mesh_vector
    h::Mesh.mesh_scalar

    rho::Mesh.mesh_scalar
    rhoU::Mesh.mesh_vector
    rhoE::Mesh.mesh_scalar

    drho::Mesh.mesh_scalar
    drhoU::Mesh.mesh_scalar
    drhoE::Mesh.mesh_scalar

    function rho_ideal_gas(mesh::Mesh.mesh_format,fluid::fluid_const,rho::Float64,T::Float64, U::Vector{Float64})
        temp = new(fluid)
        temp.rho = Mesh.mesh_scalar(mesh,rho)
        temp.T = Mesh.mesh_scalar(mesh,T)
        temp.U = Mesh.mesh_vector(mesh,U)
        P = rho*fluid.R*T
        #todo: optimize getting U2, need a vector library again it seems
        h = fluid.Cp*T

        temp.h = Mesh.mesh_scalar(mesh,h)
        temp.P = Mesh.mesh_scalar(mesh,P)
        temp.rhoU = Mesh.mesh_vector(mesh,U*rho)
        temp.U = temp.rhoU/temp.rho
        temp.rhoE = Mesh.mesh_scalar(mesh,rho*(h + 0.5*norm(U)*norm(U)  - P/rho))

        temp.drho = Mesh.mesh_scalar(mesh,0.0)
        temp.drhoU = Mesh.mesh_scalar(mesh,0.0)
        temp.drhoE = Mesh.mesh_scalar(mesh,0.0)

        return temp
    end
end

#thermo update, need to be able to parametarize this based on the gas thermodynamic definition
function update_thermo!(thermo::thermoType,mesh::Mesh.mesh_format)
    thermo.U = thermo.rhoU / thermo.rho
    #this needs better handling I think, eliminate pressure from here
    thermo.h = thermo.rhoE / thermo.rho #- 0.5 * Mesh.magSqr(thermo.U) #+ thermo.P/thermo.rho
    thermo.T = thermo.h / thermo.fluid.Cp
    thermo.P = thermo.rho*thermo.T*thermo.fluid.R
    #update_boundary_vals!(thermo,mesh)
end

function update_boundary_vals!(thermo::thermoType,mesh::Mesh.mesh_format)
    for boundaryIter in mesh.boundaries
        if boundaryIter.b_type != "empty"
            for cellIter in boundaryIter.cells
                boundaryIter.updateFunction(mesh,thermo,cellIter.id)
            end
        end
    end
end
end
