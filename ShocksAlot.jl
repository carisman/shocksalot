
#ultimately this whole thing needs to be packaged up so we're getting a bit hacky with modules
#right now
#not sure im using files right either.......

#need the revise package: press ']' then type 'add Revise' in the terminal
#this makes sure modules imported are updated when terminal is run
#otherwise they may not be reloaded
#cannot pickup on all all changes but gets most of them
#useful for developing so not constantly reloading the terminal workspace
using Revise
using Plots
using GR
push!(LOAD_PATH, "./")
#using or import? still dont understand the diff
import Mesh
import Thermo
import Flux

#include("Mesh.jl")
#include("Thermo.jl")

mesh = Mesh.mesh_format(1)
Mesh.Read_openFOAM_Mesh!(mesh)

#air constants
MW = 28.9
gamma = 1.4
mu = 0.0
k = 0.0

rhoInit = 1.8
Tinit = 300.0
Uinit = [1735,0.0,0.0]
#init fluid
air = Thermo.fluid_const(MW,gamma,mu,k)
thermo = Thermo.rho_ideal_gas(mesh,air,rhoInit,Tinit,Uinit)

Thermo.update_thermo!(thermo,mesh)
#Flux.update_flux!(thermo,mesh)

#plot variables
x = Vector{Float64}()
y = Vector{Float64}()

for cellIter in mesh.cells
    push!(x,cellIter.centroid[1])
    push!(y,cellIter.centroid[2])
end

z = thermo.T.cellVals

p = Plots.heatmap(x,y,z)
#repl is dumb and julia doesn't like outputing circular refs
print("done")
